//
//  AppDelegate.h
//  CollectionViewMayham
//
//  Created by James Cash on 17-11-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

