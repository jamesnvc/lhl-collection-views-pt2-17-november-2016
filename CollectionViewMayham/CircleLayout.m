//
//  CircleLayout.m
//  CollectionViewMayham
//
//  Created by James Cash on 17-11-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import "CircleLayout.h"

@interface CircleLayout ()

@property (nonatomic,strong) NSArray<NSArray<NSValue*>*>* points;

@end

@implementation CircleLayout

- (void)prepareLayout
{
    CGFloat sectionRadius = self.sectionRadius == 0 ? 100 : self.sectionRadius;
    NSInteger nsections = self.collectionView.numberOfSections;
    NSMutableArray *points = [[NSMutableArray alloc] initWithCapacity:nsections];

    CGFloat viewCenterX = CGRectGetMidX(self.collectionView.frame);
    CGFloat viewCenterY = CGRectGetMidY(self.collectionView.frame);

    for (int s = 0; s < nsections; s++) {
        NSInteger nitems = [self.collectionView numberOfItemsInSection:s];
        NSMutableArray *sectionPoints = [[NSMutableArray alloc] initWithCapacity:nitems];
        CGFloat r = sectionRadius * (1+s);
        CGFloat θ = 2*M_PI / nitems;
        for (int i = 0; i < nitems; i++) {
            CGFloat x = r * cos(θ*i) + viewCenterX;
            CGFloat y = r * sin(θ*i) + viewCenterY;
            CGPoint itemCenter = CGPointMake(x, y);
            [sectionPoints addObject:[NSValue valueWithCGPoint:itemCenter]];
        }
        [points addObject:sectionPoints];
    }
    self.points = [points copy];
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewLayoutAttributes *attrs = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];

    NSValue *pointValue = self.points[indexPath.section][indexPath.item];
    CGPoint point = pointValue.CGPointValue;
    attrs.center = point;
    CGFloat viewCenterX = CGRectGetMidX(self.collectionView.frame);
    CGFloat r = self.sectionRadius * (1+indexPath.section);
    CGFloat theta = acos((point.x - viewCenterX) / r);
    attrs.transform = CGAffineTransformMakeRotation(M_PI_2 - theta);
    attrs.size = CGSizeMake(50, 50);

    return attrs;
}

- (NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSMutableArray *attributes = [[NSMutableArray alloc] init];
    for (int s = 0; s < self.collectionView.numberOfSections; s++) {
        for (int i = 0; i < [self.collectionView numberOfItemsInSection:s]; i++) {
            [attributes addObject:[self layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForItem:i inSection:s]]];
        }
    }
    return attributes;
}

@end
