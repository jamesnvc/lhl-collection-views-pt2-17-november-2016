//
//  ViewController.m
//  CollectionViewMayham
//
//  Created by James Cash on 17-11-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "CircleLayout.h"

@interface ViewController () <UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) CircleLayout *circleLayout;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    self.circleLayout = [[CircleLayout alloc] init];
    self.circleLayout.sectionRadius = 200;
    self.collectionView.collectionViewLayout = self.circleLayout;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 3;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return section + 8;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CellyCell" forIndexPath:indexPath];
    UILabel *label = [cell viewWithTag:1];
    label.text = [NSString stringWithFormat:@"%ld,%ld", indexPath.section, indexPath.item];

    return cell;
}

@end
